# Toast #

### What is this repository for? ###

This code solves a problem of sequencing operations (toasting bread on a grill) [posted on Mathematics StackExchange](https://math.stackexchange.com/questions/3632331/3-toast-problem-from-thinking-mathematically-solution-check/). The problem is modeled as a constraint optimization problem using CPOptimizer.

The code was developed using CPLEX Studio 12.10 but will run with at least some earlier versions.

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

