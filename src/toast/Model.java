package toast;

import ilog.concert.IloCumulFunctionExpr;
import ilog.concert.IloException;
import ilog.concert.IloIntervalVar;
import ilog.cp.IloCP;
import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Model implements a CP model for the toast problem.
 *
 * The capacity of the toaster and the various operation times are static
 * constants, and can be adjusted. The assumption of a single cook is
 * "hard-coded" in the constraints and cannot be changed easily.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Model implements AutoCloseable {
  /* Problem parameters */
  /** Toaster capacity. */
  private static final int CAPACITY = 2;
  /** Number of slices of bread to toast. */
  private static final int SLICES = 3;
  /** Time to toast one side. */
  private static final int TOAST = 30;
  /** Time to insert a slice. */
  private static final int INSERT = 5;
  /** Time to remove a slice. */
  private static final int REMOVE = 5;
  /** Time to flip a slice. */
  private static final int FLIP = 3;

  /* CP Optimizer objects. */
  private final IloCP model;                // the model object
  private final IloIntervalVar done;        // completion of the toasting
  private final IloIntervalVar[][] toast;   // toast[i][j] = time side j of
                                            // slice i is toasting
  private final IloIntervalVar[][] insert;  // insert[i][j] = time slice i is
                                            // being inserted with side j up
  private final IloIntervalVar[][] remove;  // remove[i][j] = time slice i is
                                            // being removed after toasting
                                            // side j
  private final IloIntervalVar[] flip;      // flip[i] = time slice i is 
                                            // being flipped
  private final IloIntervalVar[] reverse;   // time i is either being flipped or
                                            // being reinserted
  private final IloCumulFunctionExpr grillUse;
                                            // grill usage as a function of time

  /**
   * Constructor.
   * @throws IloException if CPLEX balks at anything
   */
  public Model() throws IloException {
    // Instantiate the model.
    model = new IloCP();
    // Instantiate variables.
    done = model.intervalVar(0, "Completion");
    toast = new IloIntervalVar[SLICES][2];
    insert = new IloIntervalVar[SLICES][2];
    remove = new IloIntervalVar[SLICES][2];
    flip = new IloIntervalVar[SLICES];
    reverse = new IloIntervalVar[SLICES];
    for (int i = 0; i < SLICES; i++) {
      for (int j = 0; j < 2; j++) {
        toast[i][j] = model.intervalVar(TOAST, "Toast_" + i + "_" + j);
        insert[i][j] = model.intervalVar(INSERT, "Insert_" + i + "_" + j);
        remove[i][j] = model.intervalVar(REMOVE, "Remove_" + i + "_" + j);
        flip[i] = model.intervalVar(FLIP, "Flip_" + i);
        reverse[i] = model.intervalVar("Reverse_" + i);
      }
    }
    // Flipping and removing/reinserting a slice after toasting the first side
    // are optional. Either reinsertion of flipping must occur. This is modeled
    // by making flipping and reinsertation alternatives for the (mandatory)
    // "reverse" task.
    for (int i = 0; i < SLICES; i++) {
      flip[i].setOptional();
      remove[i][0].setOptional();
      insert[i][1].setOptional();
      // Reversing means either flipping or reinserting.
      model.add(model.alternative(reverse[i],
                                  new IloIntervalVar[] {flip[i],
                                                        insert[i][1]}));
      // Reinserting requires removing (and vice versa).
      model.addEq(model.presenceOf(insert[i][1]),
                  model.presenceOf(remove[i][0]));
    }
    // The objective is to minimize the completion time.
    model.addMinimize(model.endOf(done));
    // Add sequencing constraints for each slice.
    for (int i = 0; i < SLICES; i++) {
      // Must insert before toasting the first side.
      model.add(model.endBeforeStart(insert[i][0], toast[i][0]));
      // Must toast the first side before removing removing (if removal occurs).
      model.add(model.endBeforeStart(toast[i][0], remove[i][0]));
      // Must toast the first side before reversing.
      model.add(model.endBeforeStart(toast[i][0], reverse[i]));
      // Must reverse (flip or reinsert) before toasting the second side.
      model.add(model.endBeforeStart(reverse[i], toast[i][1]));
      // If a slice is removed and reinserted, removal must occur first.
      model.add(model.endBeforeStart(remove[i][0], insert[i][1]));
      // Must toast the second side before final removal.
      model.add(model.endBeforeStart(toast[i][1], remove[i][1]));
      // Each slice must be removed (after toasting the second side) before
      // we can say we are done.
      model.add(model.endBeforeStart(remove[i][1], done));
    }
    // Make a list of tasks (insertion, removal, flipping) that occupy the cook.
    ArrayList<IloIntervalVar> handsOn = new ArrayList<>();
    for (int i = 0; i < SLICES; i++) {
      handsOn.add(insert[i][0]);
      handsOn.add(reverse[i]);
      handsOn.add(remove[i][1]);
    }
    // Tasks requiring the cook cannot overlap.
    model.add(model.noOverlap(handsOn));
    // Create an expression to track grill use.
    grillUse = model.cumulFunctionExpr();
    for (int i = 0; i < SLICES; i++) {
      // Each slice occupies one unit of grill space from the time of insertion
      // to the time of removal.
      grillUse.add(model.stepAtStart(insert[i][0], 1));
      grillUse.add(model.stepAtStart(insert[i][1], 1));
      grillUse.sub(model.stepAtEnd(remove[i][0], 1));
      grillUse.sub(model.stepAtEnd(remove[i][1], 1));
    }
    // Limit grill use to the grill's capacity.
    model.add(model.le(grillUse, CAPACITY));
    // To mitigate symmetry, assume that slices are started in slice order.
    for (int i = 1; i < SLICES; i++) {
      model.add(model.startBeforeStart(insert[i - 1][0], insert[i][0]));
    }
  }

  /**
   * Runs the model and returns the objective value (total time).
   * @return the objective value
   * @throws IloException if anything goes wrong
   */
  public double solve() throws IloException {
    boolean ran = model.solve();
    if (ran) {
      return model.getObjValue();
    } else {
      throw new IloException("No solution!");
    }
  }

  /**
   * Gets a string describing the sequence of operations.
   * @return a string containing the schedule
   * @throws IloException if the solution cannot be recovered
   */
  public String getSolution() throws IloException {
    TreeSet<Event> calendar = new TreeSet<>();
    // Get the values of all present interval variables and add them to
    // the calendar.
    int endTime = model.getEnd(done);
    calendar.add(new Event("Completion", endTime));
    for (int i = 0; i < SLICES; i++) {
      calendar.add(new Event("Insert " + i,
                             model.getStart(insert[i][0])));
      if (model.isPresent(flip[i])) {
        calendar.add(new Event("Flip " + i, model.getStart(flip[i])));
      } else {
        calendar.add(new Event("Remove " + i + " to flip",
                               model.getStart(remove[i][0])));
        calendar.add(new Event("Reinsert " + i, model.getStart(insert[i][1])));
      }
      calendar.add(new Event("Remove " + i, model.getStart(remove[i][1])));
      calendar.add(new Event("Cook " + i + " front",
                             model.getStart(toast[i][0])));
      calendar.add(new Event("Cook " + i + " back",
                             model.getStart(toast[i][1])));
    }
    StringBuilder sb = new StringBuilder();
    calendar.stream().forEach((e) -> sb.append(String.format("%3d",
                                                             e.getWhen()))
                                       .append(": ")
                                       .append(e.getWhat())
                                       .append("\n"));
    sb.append("\nGrill use =\n");
    // Get the event times (without duplicates).
    int[] epochs = calendar.stream()
                           .mapToInt(e -> e.getWhen())
                           .distinct()
                           .toArray();
    // Report grill usage at the event times.
    for (int t : epochs) {
      sb.append(String.format("%3d:  %1d%n", t, model.getValue(grillUse, t)));
    }
    return sb.toString();
  }

  /**
   * Closes the model object.
   */
  @Override
  public void close() {
    model.end();
  }

  /**
   * Event is a container for an event (a combination of an operation and the
   * time at which it occurs). Events are used to convert the solution into
   * a chronological calendar of state changes.
   */
  private class Event implements Comparable<Event> {
    private final String what;
    private final int when;

    /**
     * Constructor.
     * @param operation the operation
     * @param time the time of occurrence
     */
    Event(final String operation, final int time) {
      what = operation;
      when = time;
    }

    /**
     * Gets the operation.
     * @return the operation
     */
    public String getWhat() {
      return what;
    }

    /**
     * Gets the time.
     * @return the event time
     */
    public int getWhen() {
      return when;
    }

    /**
     * Compares this event to another chronologically,
     * breaking ties alphabetically.
     * @param o the other event
     * @return -1 if this occurs first, 0 if they are simultaneous, +1 if
     * the other occurs first
     */
    @Override
    public int compareTo(final Event o) {
      int x = Integer.compare(this.getWhen(), o.getWhen());
      if (x == 0) {
        x = this.getWhat().compareToIgnoreCase(o.getWhat());
      }
      return x;
    }

  }
}
