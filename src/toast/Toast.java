package toast;

import ilog.concert.IloException;

/**
 * Toast solves a "3 Toast Problem" from Mathematics StackExchange
 * (https://math.stackexchange.com/questions/3632331/3-toast-problem-from-
 * thinking-mathematically-solution-check).
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Toast {

  /**
   * Dummy constructor.
   */
  private Toast() { }

  /**
   * Builds and solves a CP model for the problem.
   * @param args the command line arguments ignored
   */
  public static void main(final String[] args) {
    try (Model model = new Model()) {
      double time = model.solve();
      System.out.println("Optimal overall time = "
                         + String.format("%.0f", time));
      System.out.println("\nSchedule:");
      System.out.println(model.getSolution());
    } catch (IloException ex) {
      System.err.println("CPLEX failure:\n" + ex.getMessage());
    }
  }

}
